package com.example.ejemploroom

import android.app.AlertDialog
import android.content.DialogInterface
import android.os.Bundle
import android.text.Editable
import android.text.InputType
import android.view.*
import androidx.fragment.app.Fragment
import android.widget.EditText
import androidx.constraintlayout.widget.ConstraintSet
import androidx.navigation.fragment.findNavController
import androidx.recyclerview.widget.GridLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.example.ejemploroom.databinding.FragmentContactListBinding
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch
import kotlinx.coroutines.withContext

class ContactListFragment : Fragment(), OnClickListener {

    private lateinit var contactAdapter: ContactAdapter
    private lateinit var mLayoutManager: RecyclerView.LayoutManager
    private lateinit var binding: FragmentContactListBinding

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setHasOptionsMenu(true)
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?,
                              savedInstanceState: Bundle?): View? {
        binding = FragmentContactListBinding.inflate(layoutInflater)
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        setupRecyclerView()
        binding.fabAdd.setOnClickListener {
            findNavController().navigate(R.id.action_contactListFragment_to_addContactFragment)
        }
    }

    private fun setupRecyclerView() {
        contactAdapter = ContactAdapter(mutableListOf(), this)
        mLayoutManager = GridLayoutManager(context,2)
        CoroutineScope(Dispatchers.Main).launch {
            val contactList = withContext(Dispatchers.IO) { ContactApplication.database.contactDao().getAllContacts() }
            contactAdapter.setContactList(contactList)
            binding.recycler.apply {
                setHasFixedSize(true)
                layoutManager = mLayoutManager
                adapter = contactAdapter
            }
        }
    }

    override fun onCreateOptionsMenu(menu: Menu, inflater: MenuInflater) {
        inflater.inflate(R.menu.menu, menu)
        super.onCreateOptionsMenu(menu, inflater)
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        when(item.itemId){
            R.id.show_all -> selectAll()
            R.id.show_favs -> selectFavorites()
        }
        return super.onOptionsItemSelected(item)
    }

    fun selectAll(){
        CoroutineScope(Dispatchers.Main).launch {
            val contactList = withContext(Dispatchers.IO) { ContactApplication.database.contactDao().getAllContacts() }
            contactAdapter.setContactList(contactList)
        }
    }

    fun selectFavorites(){
        CoroutineScope(Dispatchers.Main).launch {
            val contactList = withContext(Dispatchers.IO) { ContactApplication.database.contactDao().getFavoriteContacts() }
            contactAdapter.setContactList(contactList)
        }
    }

    /*
    * OnClickListener
     */
    override fun onClick(contact: ContactEntity) {
        val action = ContactListFragmentDirections.actionContactListFragmentToEditContactFragment(contact)
        findNavController().navigate(action)
    }

    override fun onClickDelete(contact: ContactEntity) {
        CoroutineScope(Dispatchers.Main).launch {
            withContext(Dispatchers.IO) { ContactApplication.database.contactDao().deleteContact(contact) }
            contactAdapter.delete(contact)
        }
    }

    override fun onClickFavorite(contact: ContactEntity) {
        CoroutineScope(Dispatchers.Main).launch {
            contact.favorite = !contact.favorite
            withContext(Dispatchers.IO) { ContactApplication.database.contactDao().updateContact(contact) }
            //contactAdapter.delete(contact)
        }
    }
}