package com.example.ejemploroom

interface OnClickListener {
    fun onClick(contact: ContactEntity)
    fun onClickDelete(contact: ContactEntity)
    fun onClickFavorite(contact: ContactEntity)
}