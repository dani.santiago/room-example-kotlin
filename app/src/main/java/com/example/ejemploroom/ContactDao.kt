package com.example.ejemploroom

import androidx.room.*

@Dao
interface ContactDao {
    @Query("SELECT * FROM ContactEntity")
    fun getAllContacts(): MutableList<ContactEntity>

    @Query("SELECT * FROM ContactEntity WHERE favorite")
    fun getFavoriteContacts(): MutableList<ContactEntity>

    @Insert
    fun addContact(contactEntity: ContactEntity)

    @Update
    fun updateContact(contactEntity: ContactEntity)

    @Delete
    fun deleteContact(contactEntity: ContactEntity)
}