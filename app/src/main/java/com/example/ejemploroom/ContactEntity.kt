package com.example.ejemploroom

import android.os.Parcelable
import androidx.room.Entity
import androidx.room.PrimaryKey
import kotlinx.parcelize.Parcelize

@Parcelize
@Entity(tableName = "ContactEntity")
data class ContactEntity(
    @PrimaryKey(autoGenerate = true) var id: Long = 0,
    var name:String,
    var phoneNumber: String,
    var favorite: Boolean): Parcelable
