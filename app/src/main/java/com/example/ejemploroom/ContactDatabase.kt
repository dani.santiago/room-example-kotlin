package com.example.ejemploroom

import androidx.room.Database
import androidx.room.RoomDatabase

@Database(entities = arrayOf(ContactEntity::class), version = 2)
abstract class ContactDatabase: RoomDatabase() {
    abstract fun contactDao(): ContactDao
}