package com.example.ejemploroom

import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.navigation.fragment.findNavController
import com.example.ejemploroom.databinding.FragmentAddContactBinding
import com.example.ejemploroom.databinding.FragmentContactListBinding
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch
import kotlinx.coroutines.withContext

class AddContactFragment : Fragment() {

    private lateinit var binding: FragmentAddContactBinding

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?): View? {
        binding = FragmentAddContactBinding.inflate(layoutInflater)
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        binding.buttonAdd.setOnClickListener {
            val contactName = binding.etName.text.toString().trim()
            val phoneNumber = binding.etTelefon.text.toString().trim()
            val newContact = ContactEntity(name = contactName, phoneNumber = phoneNumber, favorite = false)
            insertContact(newContact)
            findNavController().navigate(R.id.action_addContactFragment_to_contactListFragment)
        }
    }

    private fun insertContact(contact: ContactEntity) {
        CoroutineScope(Dispatchers.IO).launch {
            ContactApplication.database.contactDao().addContact(contact)
        }
    }
}