package com.example.ejemploroom

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.example.ejemploroom.databinding.ContactViewBinding

class ContactAdapter(private var contacts: MutableList<ContactEntity>, private var listener: OnClickListener): RecyclerView.Adapter<ContactAdapter.ViewHolder>() {

    private lateinit var context: Context

    inner class ViewHolder(view: View): RecyclerView.ViewHolder(view) {
        val binding = ContactViewBinding.bind(view)

        fun setListener(contact: ContactEntity){
            binding.root.setOnClickListener {
                listener.onClick(contact)
            }
            binding.fabDelete.setOnClickListener {
                listener.onClickDelete(contact)
            }
            binding.cbFavorite.setOnClickListener {
                listener.onClickFavorite(contact)
            }
        }
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        context = parent.context
        val view = LayoutInflater.from(context).inflate(R.layout.contact_view, parent, false)
        return ViewHolder(view)
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        val contact = contacts[position]
        with(holder){
            setListener(contact)
            binding.tvName.text = contact.name
            binding.cbFavorite.isChecked = contact.favorite
        }
    }

    override fun getItemCount(): Int {
        return contacts.size
    }

    fun setContactList(contactList: MutableList<ContactEntity>){
        this.contacts = contactList
        notifyDataSetChanged()
    }

    fun delete(contact: ContactEntity) {
        val index = contacts.indexOf(contact)
        if(index != -1){
            contacts.removeAt(index)
            notifyItemRemoved(index)
        }
    }
}