package com.example.ejemploroom

import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.navigation.fragment.findNavController
import com.example.ejemploroom.databinding.FragmentEditContactBinding
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch

class EditContactFragment : Fragment() {

    lateinit var binding: FragmentEditContactBinding

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?): View? {
        binding = FragmentEditContactBinding.inflate(layoutInflater)
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        val contact = arguments?.get("contact") as ContactEntity
        binding.etName.setText(contact.name)
        binding.etTelefon.setText(contact.phoneNumber)
        binding.buttonAdd.setOnClickListener {
            contact.name = binding.etName.text.toString()
            contact.phoneNumber = binding.etTelefon.text.toString()
            updateContact(contact)
            findNavController().navigate(R.id.action_editContactFragment_to_contactListFragment)
        }
    }

    private fun updateContact(contact: ContactEntity) {
        CoroutineScope(Dispatchers.IO).launch {
            ContactApplication.database.contactDao().updateContact(contact)
        }
    }
}