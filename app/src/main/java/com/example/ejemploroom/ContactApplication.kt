package com.example.ejemploroom

import android.app.Application
import androidx.room.Room
import androidx.room.RoomDatabase

class ContactApplication: Application() {
    companion object {
        lateinit var database: ContactDatabase
    }

    override fun onCreate() {
        super.onCreate()
        database = Room.databaseBuilder(this,
            ContactDatabase::class.java,
            "ContactDatabase").build()
    }
}